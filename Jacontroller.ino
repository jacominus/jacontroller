#include "Jacontroller.h"

Adafruit_MCP23X17 *mcp[HOW_MANY_MCP23017];

Encoder myEnc(4, 5);

bool jacoMatrix[X_MATRIX_SIZE][Y_MATRIX_SIZE][Z_MATRIX_MEMO];

long jacoWheel[2];

long jacoLed;
bool tokenLed;

void setup() {
  // Set Serial COM
  Serial.begin(9600);

  // Set I2C COM frequency
  //Wire.setClock(1700000);

  // Set MCP23017 pins mode
  for (int i = 0; i < HOW_MANY_MCP23017; i++) {
    mcp[i] = new Adafruit_MCP23X17;
    while(!mcp[i]->begin_I2C(MCP23017_ADDR[i]));
    for (int j = 0; j < HOW_MANY_PINS_ON_MCP23017; j++) {
      if (i == 0) {
        mcp[i]->pinMode(j, INPUT_PULLUP);
      }
      else {
        mcp[i]->pinMode(j, OUTPUT);
        mcp[i]->digitalWrite(j, LOW);
      }
    }
  }

  // Init jacoMatrix
  for (int x = 0; x < X_MATRIX_SIZE; x++) {
    for (int y = 0; y < Y_MATRIX_SIZE; y++) {
      for (int z = 0; z < Z_MATRIX_MEMO; z++) {
        jacoMatrix[x][y][z] = true;
      }
    }
  }

  // Init jacoWheel
  for (int i = 0; i < 2; i++)
    jacoWheel[i] = i * (-1);

  // Waiting for Pd
  pinMode(LED_BUILTIN, OUTPUT);
  while (!usbMIDI.read())
    digitalWrite(LED_BUILTIN, int((millis() / 1000) % 2));

  // End of setup
  long currentTime = millis();
  while (millis() - currentTime < WAITING_TIME)
    digitalWrite(LED_BUILTIN, int((millis() / 25) % 2));
  digitalWrite(LED_BUILTIN, LOW);
}

void loop() {
  // JacoMatrix
  for (int x = 0; x < X_MATRIX_SIZE; x++) {
    for (int y = 0; y < Y_MATRIX_SIZE; y++) {
      mcp[1]->digitalWrite(y, LOW);
      if ((jacoMatrix[x][y][CURRENT] = mcp[0]->digitalRead(x)) != jacoMatrix[x][y][LAST]) {
        // MIDI-CC
        //usbMIDI.sendControlChange(x + 10 * y, jacoMatrix[x][y][CURRENT] * 127, 0, 0);
        // Note-on / Note-off
        if (!jacoMatrix[x][y][CURRENT])
          usbMIDI.sendNoteOn(x + 10 * y, 127, 0, 0);
        else
          usbMIDI.sendNoteOff(x + 10 * y, 0, 0, 0);
        digitalWrite(LED_BUILTIN, HIGH);
        jacoLed = millis();
        tokenLed = true;
      }
      jacoMatrix[x][y][LAST] = jacoMatrix[x][y][CURRENT];
      mcp[1]->digitalWrite(y, HIGH);
    }
  }

  // JacoWheel
  if ((jacoWheel[CURRENT] = myEnc.read()) != jacoWheel[LAST]) {
    usbMIDI.sendControlChange(101, (float)(constrain(jacoWheel[CURRENT] - jacoWheel[LAST], -JACO_WHEEL_THRESH, JACO_WHEEL_THRESH) + JACO_WHEEL_THRESH) / (JACO_WHEEL_THRESH * 2) * 127, 0, 0);
    digitalWrite(LED_BUILTIN, HIGH);
    jacoLed = millis();
    tokenLed = true;
  }
  jacoWheel[LAST] = jacoWheel[CURRENT];

  // Update LED
  if (millis() - jacoLed > 100 && tokenLed) {
     digitalWrite(LED_BUILTIN, LOW);
     tokenLed = false;
  }

  // MIDI Controllers should discard incoming MIDI messages.
  // http://forum.pjrc.com/threads/24179-Teensy-3-Ableton-Analog-CC-causes-midi-crash
  while (usbMIDI.read()) {
    // ignore incoming messages
  }
}
