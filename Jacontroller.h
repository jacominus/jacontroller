#include <Wire.h>
#include <Adafruit_MCP23X17.h>

#include <Encoder.h>

#define HOW_MANY_MCP23017 2
#define HOW_MANY_PINS_ON_MCP23017 16

#define X_MATRIX_SIZE 10  // Max. 16
#define Y_MATRIX_SIZE 10  // Max. 16

#define Z_MATRIX_MEMO 2

#define JACO_WHEEL_THRESH 500

#define WAITING_TIME 1000

const uint8_t MCP23017_ADDR[] = { 0x20, 0x21 };

enum MEMORY {
  CURRENT,
  LAST
};
